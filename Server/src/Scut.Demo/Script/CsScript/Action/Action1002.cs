﻿using GameServer.Script.Model;
using System;
using ZyGames.Framework.Common.Security;
using ZyGames.Framework.Common.Serialization;
using ZyGames.Framework.Game.Lang;
using ZyGames.Framework.Game.Runtime;
using ZyGames.Framework.Game.Service;
using ZyGames.Framework.Game.Sns;

namespace GameServer.Script.CsScript.Action
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>继续BaseStruct类:不检查用户合法性请求;AuthorizeAction:有验证合法性</remarks>
    public class Action1002 : BaseStruct
    {
        private string passport = string.Empty;
        private string password = string.Empty;
        private string deviceID = string.Empty;
        private int mobileType = 0;
        private int gameID = 0;
        private string retailID = string.Empty;
        private string clientAppVersion = string.Empty;
        private int ScreenX = 0;
        private int ScreenY = 0;

        public Action1002(ActionGetter actionGetter)
            : base((short)ActionType.Regist, actionGetter)
        {
        }

        /// <summary>
        /// 客户端请求的参数较验
        /// </summary>
        /// <returns>false:中断后面的方式执行并返回Error</returns>
        public override bool GetUrlElement()
        {
            if (httpGet.GetInt("MobileType", ref mobileType) &&
                httpGet.GetInt("GameID", ref gameID) &&
                httpGet.GetString("RetailID", ref retailID) &&
                httpGet.GetString("ClientAppVersion", ref clientAppVersion) &&
                httpGet.GetString("DeviceID", ref deviceID))
            {
                httpGet.GetInt("ScreenX", ref ScreenX);
                httpGet.GetInt("ScreenY", ref ScreenY);
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 业务逻辑处理
        /// </summary>
        /// <returns>false:中断后面的方式执行并返回Error</returns>
        public override bool TakeAction()
        {
            try
            {
                string[] userList = SnsManager.GetRegPassport(deviceID);
                passport = userList[0];
                password = userList[1];
                return true;
            }
            catch (Exception ex)
            {
                this.SaveLog(ex);
                this.ErrorCode = Language.Instance.ErrorCode;
                this.ErrorInfo = Language.Instance.St1002_GetRegisterPassportIDError;
                return false;
            }
        }
        /// <summary>
        /// 下发给客户的包结构数据
        /// </summary>
        public override void BuildPacket()
        {
            PushIntoStack(passport);
            PushIntoStack(password);
        }

    }

    public class Action1003 : BaseStruct
    {
        Request1001Pack requestPack;
        public Action1003(ActionGetter actionGetter)
            : base((short)ActionType.Regist, actionGetter)
        {
            Console.WriteLine("解析协议体 actionGetter 成功:{0}", actionGetter.GetMessage().ToString());
        }
        public override bool GetUrlElement()
        {
            byte[] data = (byte[])actionGetter.GetMessage();
            if (data.Length > 0)
            {
                requestPack = ProtoBufUtils.Deserialize<Request1001Pack>(data);
                Console.WriteLine("解析协议体 Request1001Pack.PageSize:{0},成功", requestPack.PageSize);
                return true;

            }
            return false;
        }
        public override bool TakeAction()
        {
            Console.WriteLine("执行自定义逻辑 Request1001Pack.PageSize:{0},成功", requestPack.PageSize);
            return true;
        }
        public override void BuildPacket()
        {
            Console.WriteLine("下发数据 Request1001Pack.PageSize:{0},成功", requestPack.PageSize);
            PushIntoStack(requestPack.PageSize);
            base.BuildPacket();
        }
    }
}
